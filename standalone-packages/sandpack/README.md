## GitLab fork of Codesandbox Client's sandpack

This is a temporary fork of the
[codesandbox-client](https://github.com/codesandbox/codesandbox-client/) repo
which will be used to immediately fix some issues with GitLab's Live Preview
feature.

See also:

- [Original README](https://github.com/codesandbox/codesandbox-client/blob/838d2aead016d9d66d47c7d452ca16e3231d5dc5/standalone-packages/sandpack/README.md)
- [Upstream PR to Codesandbox Client](https://github.com/codesandbox/codesandbox-client/pull/5137)
- [GitLab Epic for Live Preview in Web IDE](https://gitlab.com/groups/gitlab-org/-/epics/3138)
